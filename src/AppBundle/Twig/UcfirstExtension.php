<?php
namespace AppBundle\Twig;

class UcfirstExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ucfirst', array($this, 'ucfirstFilter'), array('needs_environment' => true)),
        );
//        return array(
//            new Twig_SimpleFilter('ucfirst',
//                array($this, 'ucFirst'), array('needs_environment' => true)
//            ),
//        );
    }

    public function ucfirstFilter(\Twig_Environment $env, $string)
    {
        if (null !== $charset = $env->getCharset()) {
            $prefix = mb_strtoupper(mb_substr($string, 0, 1, $charset), $charset);
            $suffix = mb_substr($string, 1, mb_strlen($string, $charset));
            return sprintf('%s%s', $prefix, $suffix);
        }
        return ucfirst(mb_strtolower($string));
    }

//    public function ucfirstFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
//    {
//        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
//        $price = '$'.$price;
//
//        return $price;
//    }
}