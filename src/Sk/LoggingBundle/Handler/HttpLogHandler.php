<?php
namespace Sk\LoggingBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Query;
use Sk\LoggingBundle\Entity\HttpLog;
use Sk\LoggingBundle\Repository\HttpLogRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HttpLogHandler
{
    private $om;
    private $entityClass;
    /** @var $repository HttpLogRepository */
    private $repository;
    private $logHeaderWord;
    private $kernel;

    public function __construct(
        ObjectManager $om,
        $entityClass,
        $logHeaderWord,
        $kernel
    )
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->logHeaderWord = $logHeaderWord;
        $this->kernel = $kernel;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function log(Request $request, Response $response)
    {
        if(!in_array($this->kernel->getEnvironment(), ['dev', 'prod'])) {
            return true;
        }

        if($request->headers->has($this->logHeaderWord)) {
            $httpLogEntity = $this->create($request, $response);
//            dump($httpLogEntity); die();
            if(!empty($httpLogEntity)) {
                $this->save($httpLogEntity);
                return true;
            }
        }

        return false;
    }

    /**
     * @param $id
     * @return HttpLog|null
     */
    public function getById($id)
    {

        /** @var  $doc HttpLog */
        $doc = $this->repository->find($id);

        if(empty($doc)) {
            return null;
        }

        return $doc;
    }

    /**
     * @param array $ids
     * @return array HttpLog
     */
    public function getByIds(array $ids)
    {
        $criteria = array(
            'id' => $ids
        );
        return $this->repository->findBy($criteria);
    }

    protected function create(Request $request, Response $response)
    {
        /** @var HttpLog $entity */
        $entity = new $this->entityClass();
        $entity->setCreatedAt(new \DateTime());
        $entity->setActiveState(1);
        $entity->setUserId(null);//@TODO: fix user later
        $entity->setHttpStatus($response->getStatusCode());
        $entity->setIp($request->getClientIp());
        $entity->setRequestHeaders($request->headers->all());
        $entity->setRequestBody($request->request->all());
        $entity->setResponseHeaders($response->headers->all());
        $entity->setResponseBody($response->getContent());
        $entity->setRequestMethod($request->getMethod());
        $url = $request->getUri();//getHttpHost();
        $entity->setUrl($url);

        return $entity;
    }

    protected function save(HttpLog $entity)
    {
        return $this->repository->save($entity);
    }

    public function getDataList(array $criteria=array(), $limit=null, $offset=null, array $sortExt=array(), array $fields = array(), $hydrationMode=Query::HYDRATE_OBJECT)
    {
        return $this->repository->findDataList($criteria, $limit, $offset, $sortExt, $fields, $hydrationMode);
    }

    public function getDataListCount(array $criteria=array(), $offset=null )
    {
        return $this->repository->findCount( $criteria, $offset);
    }

//    public function delete($criteria, $limit=null, $offset=null, array $sortExt=array())
//    {
//        $this->repository->delete($criteria, $limit, $offset, $sortExt);
//    }
}