<?php
// NotificationListener
namespace Sk\LoggingBundle\EventListener;



use Sk\LoggingBundle\Handler\HttpLogHandler;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class LogHeaderListener
{
    private $securityTokenStorage;
    private $hHttpLog;

    public function __construct($securityTokenStorage, HttpLogHandler $hHttpLog)
    {
        $this->securityTokenStorage = $securityTokenStorage;
        $this->hHttpLog = $hHttpLog;
    }
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $isHttpLoged = $this->hHttpLog->log($request, $response);
    }
}