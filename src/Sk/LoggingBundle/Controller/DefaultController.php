<?php

namespace Sk\LoggingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function addLogAction(Request $request)
    {
        $request->headers->set('httplog', 1);
        $status = [200, 201, 404, 500];
        $randStatus = $status[rand(0, 3)];
        return new Response('Test response add log'.$randStatus, $randStatus);
    }
}
