<?php

namespace Sk\LoggingBundle\Controller;

use Sk\LoggingBundle\Entity\HttpLog;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Httplog controller.
 *
 */
class HttpLogController extends Controller
{
    /**
     * Lists all httpLog entities.
     *
     */
    public function indexAction(Request $request)
    {
        $hHttpLog = $this->get('sk.httplog.service');


        if(stripos($request->getMethod(), 'post')===false) {
            $filterParams = $request->query->get('f');
        } else {
            $filterParams = $request->request->get('f');
        }

        $formOptions = [];
        $filterForm = $this->createForm('Sk\LoggingBundle\Form\HttpLogFilterType', $filterParams, $formOptions);

        $filterForm->handleRequest($request);

        $criteriFilter = [];
        if($filterForm->isSubmitted()) {
            if($filterForm->isValid()) {
                $criteriFilter['ip'] = $filterForm->getData()['ip'];
            }
        }
        $httpLogEntities = $hHttpLog->getDataList($criteriFilter);//@TODO: add paging

        return $this->render('SkLoggingBundle:HttpLog:index.html.twig', array(
            'httpLogs' => $httpLogEntities,
            'filterForm' => $filterForm->createView(),
        ));
    }
//
//    /**
//     * Creates a new httpLog entity.
//     *
//     */
//    public function newAction(Request $request)
//    {
//        $httpLog = new Httplog();
//        $form = $this->createForm('Sk\LoggingBundle\Form\HttpLogType', $httpLog);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($httpLog);
//            $em->flush();
//
//            return $this->redirectToRoute('sk_http-log_show', array('id' => $httpLog->getId()));
//        }
//
//        return $this->render('httplog/new.html.twig', array(
//            'httpLog' => $httpLog,
//            'form' => $form->createView(),
//        ));
//    }
//
//    /**
//     * Finds and displays a httpLog entity.
//     *
//     */
//    public function showAction(HttpLog $httpLog)
//    {
//        $deleteForm = $this->createDeleteForm($httpLog);
//
//        return $this->render('httplog/show.html.twig', array(
//            'httpLog' => $httpLog,
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }
//
//    /**
//     * Displays a form to edit an existing httpLog entity.
//     *
//     */
//    public function editAction(Request $request, HttpLog $httpLog)
//    {
//        $deleteForm = $this->createDeleteForm($httpLog);
//        $editForm = $this->createForm('Sk\LoggingBundle\Form\HttpLogType', $httpLog);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('sk_http-log_edit', array('id' => $httpLog->getId()));
//        }
//
//        return $this->render('httplog/edit.html.twig', array(
//            'httpLog' => $httpLog,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }
//
//    /**
//     * Deletes a httpLog entity.
//     *
//     */
//    public function deleteAction(Request $request, HttpLog $httpLog)
//    {
//        $form = $this->createDeleteForm($httpLog);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($httpLog);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('sk_http-log_index');
//    }
//
//    /**
//     * Creates a form to delete a httpLog entity.
//     *
//     * @param HttpLog $httpLog The httpLog entity
//     *
//     * @return \Symfony\Component\Form\Form The form
//     */
//    private function createDeleteForm(HttpLog $httpLog)
//    {
//        return $this->createFormBuilder()
//            ->setAction($this->generateUrl('sk_http-log_delete', array('id' => $httpLog->getId())))
//            ->setMethod('DELETE')
//            ->getForm()
//        ;
//    }
}
