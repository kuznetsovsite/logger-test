<?php

namespace Sk\LoggingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Ip;

class HttpLogFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
//        $builder->add('url');
//        $builder->add('requestBody');
//        $builder->add('requestHeaders');
//        $builder->add('requestPost');
//        $builder->add('httpStatus');
        $builder->add('ip', TextType::class, [
            'constraints'=>[new Ip(array())]
        ]);
//        $builder->add('createdAt');
//        $builder->add('responseBody');
//        $builder->add('responseHeaders');
//        $builder->add('activeState');
//        $builder->add('userId');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null //'Sk\LoggingBundle\Entity\HttpLog'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'f';
    }


}
