<?php

namespace Sk\LoggingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HttpLog
 *
 * Data logging request and response storage
 *
 * @ORM\Table(name="sk_http_log")
 * @ORM\Entity(repositoryClass="Sk\LoggingBundle\Repository\HttpLogRepository")
 */
class HttpLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     */
    private $url;

    /**
     * @var array|null
     *
     * @ORM\Column(name="requestBody", type="json_array", nullable=true)
     */
    private $requestBody;

    /**
     * @var array|null
     *
     * @ORM\Column(name="requestHeaders", type="json_array", nullable=true)
     */
    private $requestHeaders;

    /**
     * @var string|null
     *
     * @ORM\Column(name="requestMethod", type="string", nullable=true)
     */
    private $requestMethod;

    /**
     * @var int|null
     *
     * @ORM\Column(name="httpStatus", type="integer", nullable=true)
     */
    private $httpStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=64, nullable=true)
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetimetz")
     */
    private $createdAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="responseBody", type="text", nullable=true)
     */
    private $responseBody;

    /**
     * @var array
     *
     * @ORM\Column(name="responseHeaders", type="json_array", nullable=true)
     */
    private $responseHeaders;

    /**
     * @var int|null
     *
     * @ORM\Column(name="activeState", type="integer", nullable=true)
     */
    private $activeState;

    /**
     * @var int|null
     *
     * @ORM\Column(name="userId", type="integer", nullable=true)
     */
    private $userId;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return HttpLog
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set requestBody.
     *
     * @param array|null $requestBody
     *
     * @return HttpLog
     */
    public function setRequestBody($requestBody = null)
    {
        $this->requestBody = $requestBody;

        return $this;
    }

    /**
     * Get requestBody.
     *
     * @return array|null
     */
    public function getRequestBody()
    {
        return $this->requestBody;
    }

    /**
     * Set requestHeaders.
     *
     * @param array|null $requestHeaders
     *
     * @return HttpLog
     */
    public function setRequestHeaders($requestHeaders = null)
    {
        $this->requestHeaders = $requestHeaders;

        return $this;
    }

    /**
     * Get requestHeaders.
     *
     * @return array|null
     */
    public function getRequestHeaders()
    {
        return $this->requestHeaders;
    }

    /**
     * Set requestMethod.
     *
     * @param string|null $requestMethod
     *
     * @return HttpLog
     */
    public function setRequestMethod($requestMethod = null)
    {
        $this->requestMethod = $requestMethod;

        return $this;
    }

    /**
     * Get requestMethod.
     *
     * @return string|null
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * Set httpStatus.
     *
     * @param int|null $httpStatus
     *
     * @return HttpLog
     */
    public function setHttpStatus($httpStatus = null)
    {
        $this->httpStatus = $httpStatus;

        return $this;
    }

    /**
     * Get httpStatus.
     *
     * @return int|null
     */
    public function getHttpStatus()
    {
        return $this->httpStatus;
    }

    /**
     * Set ip.
     *
     * @param string|null $ip
     *
     * @return HttpLog
     */
    public function setIp($ip = null)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return HttpLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set responseBody.
     *
     * @param string|null $responseBody
     *
     * @return HttpLog
     */
    public function setResponseBody($responseBody = null)
    {
        $this->responseBody = $responseBody;

        return $this;
    }

    /**
     * Get responseBody.
     *
     * @return string|null
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    /**
     * Set responseHeaders.
     *
     * @param array|null $responseHeaders
     *
     * @return HttpLog
     */
    public function setResponseHeaders($responseHeaders = null)
    {
        $this->responseHeaders = $responseHeaders;

        return $this;
    }

    /**
     * Get responseHeaders.
     *
     * @return array|null
     */
    public function getResponseHeaders()
    {
        return $this->responseHeaders;
    }

    /**
     * Set activeState.
     *
     * @param int|null $activeState
     *
     * @return HttpLog
     */
    public function setActiveState($activeState = null)
    {
        $this->activeState = $activeState;

        return $this;
    }

    /**
     * Get activeState.
     *
     * @return int|null
     */
    public function getActiveState()
    {
        return $this->activeState;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return HttpLog
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }
}
