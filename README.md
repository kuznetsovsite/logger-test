Лог запросов

Задача
Написать компонент для логирования http-запросов и ответов.

Функциональные требования
1. Клиент в http-запросе указывает специальный флаг. Если флаг
указан, http-запрос и результат его выполнения (http-ответ)
сохраняются в лог
2. Лог должен выводиться на html-странице по адресу
/admin/http-log. Лог должен быть представлен в виде таблицы
с полями:
url запроса
запрос (заголовки и тело),
ответ (заголовки и тело),
http статус ответа,
ip клиента,
время и дата

3. Должна быть фильтрация по IP клиента

Технические требования
1. Фреймворк Symfony
2. Компонент EventDispatcher для перехвата запросов
3. Логирование должно работать в prod и dev окружениях
(symfony environment)
4. Хранилище логов любое (на выбор исполнителя)

Результат
Программный код необходимо запушить в git-репозиторий
(предпочтительно https://bitbucket.org) и прислать ссылку.